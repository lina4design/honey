import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import "./css/basic.css";
import "./css/style.css";
import "./css/utils.css";
import "./css/fixes.css";
import { NAV_MAP } from "./globals";
import { Header, Footer } from "./components";

class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <Header />
          <div>
            {NAV_MAP.map(({ path, page }) => <Route path={path} key={path} exact render={page} />)}
          </div>
          <Footer />
        </div>
      </Router>
    );
  }
}
//= ============
ReactDOM.render(<App />, document.getElementById("root"));
