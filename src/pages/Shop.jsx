import React from "react";
import { Items, Filters } from "../components";
import { ITEMS } from "../globals";

export class Shop extends React.Component {
  render() {
    return (
      <div>
        <Filters />
        <div className="container">
          <Items itemsArray={ITEMS} />
        </div>
      </div>
    );
  }
}
