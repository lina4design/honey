import React from "react";
import { Categories, CustomCarousel, Items } from "../components";
import { CATEGORIES, IMAGES, ITEMS } from "../globals";

export class Home extends React.Component {
  render() {
    const newArray = [];
    ITEMS.map((el) => {
      if (el.tag === "new") {
        newArray.push(el);
      }
    });
    return (
      <div>
        <CustomCarousel className="section">
          {IMAGES.map(image => (
            <div>
              <div
                className="d-flex "
                style={{
                  backgroundSize : "cover",
                  backgroundImage: `url(${image})`,
                  height         : "calc(50vh - 56px )",
                }}
              >
                <div className="container carousel-content">
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit. Deserunt possimus ad,
                  voluptatum similique nobis consequatur eos placeat accusantium amet voluptas qui,
                  dicta rerum eius, perspiciatis libero voluptatibus quos error eum!
                </div>
              </div>
            </div>
          ))}
        </CustomCarousel>
        <div className="container">
          <h2>
Kategorijos
          </h2>
          <Categories categoryArray={CATEGORIES} className="section" />
          <h2>
Naujienos
          </h2>
          <Items itemsArray={newArray} className="section" />
        </div>
      </div>
    );
  }
}
