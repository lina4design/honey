import React from "react";
import ImageGallery from "react-image-gallery";
import * as Icons from "react-icons/lib/fa";
import {
  Header, CustomCarousel, Items, Categories,
} from "../components";
import { GALLERY } from "../globals";
import "react-image-gallery/styles/css/image-gallery.css";

export class ItemDesc extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity   : 1,
      inStock    : 7,
      activePlus : true,
      activeMinus: true,
      price      : 13.45,
    };
    this.onMinusOne = this.onMinusOne.bind(this);
    this.onPlusOne = this.onPlusOne.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
    this.onBuy = this.onBuy.bind(this);
  }

  onPlusOne() {
    const { quantity, inStock } = this.state;

    if (quantity >= inStock) return;

    this.setState(
      prevQuantity => ({
        quantity: prevQuantity.quantity + 1,
      }),
      this.onInputChange,
    );
  }

  onMinusOne() {
    const { quantity } = this.state;

    if (quantity <= 0) return;

    this.setState(
      prevQuantity => ({
        quantity: prevQuantity.quantity - 1,
      }),
      this.onInputChange,
    );
  }

  onInputChange() {
    const { quantity, inStock, price } = this.state;

    this.setState({
      activeMinus: quantity > 0,
      activePlus : inStock > quantity,
      sumToPay   : parseFloat(quantity * price).toFixed(2),
    });
  }

  onBuy() {
    const { inStock, quantity } = this.state;
    if (inStock > 0) {
      this.setState({
        inStock : inStock - quantity,
        quantity: 0,
      });
    }
  }

  render() {
    const {
      inStock, quantity, sumToPay, price, activeMinus, activePlus,
    } = this.state;
    return (
      <div className="flex-column">
        <div className="container shadow white-section">
          <div className="row">
            <div className="col">
              <h2>
Pavadinimas
              </h2>
              <p>
                As with all shorthand properties, any omitted sub-values will be set to their
                initial value. Importantly, border cannot be used to specify a custom value for
                border-image, but instead sets it to its initial value, i.e., none.
              </p>
              <p>
                The border shorthand is especially useful when you want all four borders to be the
                same. To make them different from each other, however, you can use the longhand
                border-width, border-style, and border-color properties, which accept different
                values for each side. Alternatively, you can target one border at a time with the
                physical (e.g., border-top ) and logical (e.g., border-block-start) border
                properties.
              </p>
              <p>
                The border shorthand is especially useful when you want all four borders to be the
                same. To make them different from each other, however, you can use the longhand
                border-width, border-style, and border-color properties, which accept different
                values for each side. Alternatively, you can target one border at a time with the
                physical (e.g., border-top ) and logical (e.g., border-block-start) border
                properties.
              </p>

              <h4 className="mt-auto">
                Liko
                {` ${inStock} `}
                vienetai
              </h4>
              <div className="buy-details">
                <div>
                  <span className="new-price">
                    {`${sumToPay || price} $`}
                  </span>
                </div>
                <div className="quantity">
                  <Icons.FaMinusCircle
                    onClick={this.onMinusOne}
                    className={activeMinus ? null : "not-active"}
                  />
                  <input defaultValue={1} value={quantity} />
                  <Icons.FaPlusCircle
                    onClick={this.onPlusOne}
                    className={activePlus ? null : "not-active"}
                  />
                </div>
                <button className="button buy-in-desc" onClick={this.onBuy}>
                  Į krepšelį
                  <Icons.FaShoppingBasket size="2rem" className="icon-in-button" />
                </button>
              </div>
            </div>
            <div className="col pt-5">
              <ImageGallery items={GALLERY} showPlayButton={false} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
