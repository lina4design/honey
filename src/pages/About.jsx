import React from "react";
import { Header } from "../components";

export class About extends React.Component {
  render() {
    return (
      <div className="flex-column">
        <div className="container white-section shadow">
          <h2>
Apie įmonę
          </h2>
          <p>
            As with all shorthand properties, any omitted sub-values will be set to their initial
            value. Importantly, border cannot be used to specify a custom value for border-image,
            but instead sets it to its initial value, i.e., none.
            <br />
            The border shorthand is especially useful when you want all four borders to be the same.
            To make them different from each other, however, you can use the longhand border-width,
            border-style, and border-color properties, which accept different values for each side.
            Alternatively, you can target one border at a time with the physical (e.g., border-top )
            and logical (e.g., border-block-start) border properties.
          </p>
        </div>
      </div>
    );
  }
}

export default About;
