import React from "react";
import { FaShoppingCart } from "react-icons/lib/fa";
import {
  About, Cart, Home, ItemDesc, Shop,
} from "./pages";

export { default as LOGO } from "./images/logo/logo.svg";
export const NAV_MAP = [
  {
    path : "/",
    title: "Pagrindinis",
    page : () => <Home />,
  },
  {
    path : "/about",
    title: "Apie",
    page : () => <About />,
  },
  {
    path : "/shop",
    title: "Prekės",
    page : () => <Shop />,
  },
  {
    path : "/cart",
    title: "Pirkiniai",
    icon : <FaShoppingCart />,
    page : () => <Cart />,
  },
  {
    path  : "/shop/new/item",
    title : "Naujienos",
    page  : () => <ItemDesc />,
    hidden: true,
  },
];

export const IMAGES = [
  require("./images/bee1.jpg"),
  require("./images/bee2.jpg"),
  require("./images/bee3.jpg"),
  require("./images/honey1.jpg"),
  require("./images/honey2.jpg"),
  require("./images/honey3.jpg"),
];

export const ITEMS = [
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
    tag      : "new",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
  {
    imagePath: IMAGES[0],
    title    : "Pavadinimas1",
    price    : "15.00$",
  },
  {
    imagePath: IMAGES[1],
    title    : "Pavadinimas2",
    price    : "9.00$",
  },
  {
    imagePath: IMAGES[2],
    title    : "Pavadinimas7",
    price    : "14.00$",
    oldPrice : "17.00$",
  },
  {
    imagePath: IMAGES[3],
    title    : "Ilgas kažkoks pavadinimas",
    price    : "7.00$",
  },
  {
    imagePath: IMAGES[4],
    title    : "Pavadinimas12",
    price    : "20.00$",
  },
  {
    imagePath: IMAGES[5],
    title    : "Pavadinimas74",
    price    : "30.00$",
  },
];
export const GALLERY = [
  {
    original : IMAGES[0],
    thumbnail: IMAGES[0],
  },
  {
    original : IMAGES[1],
    thumbnail: IMAGES[1],
  },
  {
    original : IMAGES[2],
    thumbnail: IMAGES[2],
  },
];
export const CATEGORIES = [
  {
    imagePath: IMAGES[3],
    title    : "Medus",
    white    : true,
  },
  {
    imagePath: IMAGES[1],
    title    : "Bitės",
    white    : true,
  },
  {
    imagePath: IMAGES[2],
    title    : "Gėlės",
    white    : true,
  },
  {
    imagePath: IMAGES[4],
    title    : "Kita",
    white    : false,
  },
];
export const DROPDOWNS = [
  {
    name : "Kaina",
    style: "dropdown",
    list : ["< 5$", "5$ - 10$", "10$ - 15$"],
  },
  {
    name : "Kategorija",
    id   : "category",
    style: "dropdown",
    list : ["medus", "bitės", "gėlės"],
  },
];
