import React from "react";
import { NavLink } from "react-router-dom";
import * as Icons from "react-icons/lib/fa";

export class Footer extends React.Component {
  render() {
    return (
      <div className="footer d-flex mt-auto">
        <div className="container">
          <div className="row">
            <div className="col">
              <h6>
Puslapiai
              </h6>
              <ul>
                <li>
                  <NavLink exact to="/">
                    Pagrindinis
                  </NavLink>
                </li>
                <li>
                  <NavLink exact to="/about">
                    Apie
                  </NavLink>
                </li>
                <li>
                  <NavLink exact to="/shop">
                    Prekės
                  </NavLink>
                </li>
                <li>
                  <NavLink exact to="/cart">
                    Krepšelis
                  </NavLink>
                </li>
              </ul>
            </div>
            <div className="col">
              <h6>
Kontaktai
              </h6>
              <ul>
                <li>
Tel.: +370 6 375 4085
                </li>
                <li>
Adresas: Žolyno g. 15, LT-10209 Vilnius
                </li>
                <li>
elektroninis paštas: aidas@bites.lt
                </li>
                <li className="contacts">
                  <a href="">
                    <Icons.FaFacebookSquare fontSize="25" />
                  </a>
                  <a href="">
                    <Icons.FaInstagram fontSize="25" />
                  </a>
                  <a href="">
                    <Icons.FaTwitter fontSize="25" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
