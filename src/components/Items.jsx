import React from "react";
import * as Icons from "react-icons/lib/fa";
import { NavLink } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroller";
import database from "../firebase";

export class Items extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  // componentDidMount() {
  //   const itemRef = database.firestore().collection('items');
  //   itemRef.on('value', (snapshot) => {
  //     const item = {
  //       title    : snapshot.title.val(),
  //       desc     : snapshot.desc.val(),
  //       price    : snapshot.price.val(),
  //       old_price: snapshot.oldPrice.val(),
  //     };
  //     console.log(snapshot);
  //     this.setState({});
  //   });
  // }

  render() {
    const { itemsArray } = this.props;
    return (
      <div className="row justify-content-left">
        {itemsArray.map(item => (
          <div className=" col-sm-5 col-lg-4 col-xl-3 ">
            <InfiniteScroll pageStart={0} hasMore={true || false}>
              <div className="item shadow">
                <NavLink exact to="shop/new/item">
                  <div className="item-image" style={{ backgroundImage: `url(${item.imagePath})` }}>
                    <Icons.FaSearch size="2rem" className="image-icon" />
                  </div>
                </NavLink>
                <div className="item-card d-flex flex-column">
                  <div className="">
                    <h6 className="p-0 m-0">
                      {item.title}
                    </h6>
                    <small className="mini-desc">
                      Trumpas tekstukas kažką pasakantis apie produktą...
                    </small>
                  </div>
                  <div className="flex-row price">
                    <div className="d-flex align-self-end">
                      <span className="new-price">
                        {item.price}
                      </span>
                      <span className="old-price">
                        {item.oldPrice}
                      </span>
                    </div>
                    <div className="button-group">
                      <button className="button buy ">
                        <Icons.FaShoppingBasket size={18} />
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </InfiniteScroll>
          </div>
        ))}
      </div>
    );
  }
}
