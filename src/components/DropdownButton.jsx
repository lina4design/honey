import React from "react";
import {
  Dropdown, DropdownToggle, DropdownMenu, DropdownItem,
} from "reactstrap";

export class DropdownButton extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
    };
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen,
    }));
  }

  render() {
    const { dropdownObj } = this.props;
    const { dropdownOpen } = this.state;
    const { style, name, list } = dropdownObj;
    return (
      <Dropdown isOpen={dropdownOpen} toggle={this.toggle} className="mr-3">
        <DropdownToggle caret className={style}>
          {name}
        </DropdownToggle>
        <DropdownMenu>
          {list.map(listItem => (
            <DropdownItem>
              {listItem}
            </DropdownItem>
          ))}
        </DropdownMenu>
      </Dropdown>
    );
  }
}
