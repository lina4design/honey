import React from "react";
import * as Icons from "react-icons/lib/fa";
import { DropdownButton } from "../components";
import { DROPDOWNS } from "../globals";

export class Filters extends React.Component {
  render() {
    return (
      <div className="container">
        <div className="d-flex my-3">
          <button className="button bg-blue mr-3">
            <Icons.FaThLarge />
          </button>
          <button className="button bg-blue mr-3">
            <Icons.FaThList />
          </button>
          {DROPDOWNS.map(prop => <DropdownButton dropdownObj={prop} />)}
          <div className="search">
            <Icons.FaSearch color="rgba(164, 124, 6, 0.7)" className="mr-2" size="20" />
            <input type="text" placeholder="Ieškoti" />
          </div>
        </div>
      </div>
    );
  }
}
