import React from 'react';
import Slick from 'react-slick';

export const CustomCarousel = ({ children, ...otherProps }) => {
  const settings = {
    dots          : false,
    infinite      : true,
    autoPlay      : true,
    speed         : 500,
    slidesToShow  : 1,
    slidesToScroll: 1,
    ...otherProps,
  };
  return (
    <Slick {...settings}>
      {children}
    </Slick>
  );
};
