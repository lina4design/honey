import React from 'react';

export class Categories extends React.Component {
  render() {
    const { categoryArray } = this.props;
    return (
      <div className="row">
        {categoryArray.map(category => (
          <div className="col-6 ">
            <div
              className="category d-flex shadow"
              style={{ backgroundImage: `url(${category.imagePath})` }}
            >
              <h3 style={{ color: `${category.white ? 'white' : 'black'}` }}>
                {category.title}
              </h3>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
