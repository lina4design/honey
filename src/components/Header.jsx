import React from 'react';
import { NavLink } from 'react-router-dom';
import { LOGO, NAV_MAP } from '../globals';

export class Header extends React.Component {
  render() {
    return (
      <div className="header shadow">
        <nav className="d-flex align-items-center container ">
          <NavLink to="/" exact>
            <img src={LOGO} alt="" className="logo" />
            <span className="brand">
Aido Bitės
            </span>
          </NavLink>
          <ul className="navigation">
            {NAV_MAP.map(
              ({
                path, title, icon: Icon, hidden,
              }) => (hidden ? null : (
                <li key={title}>
                  <NavLink to={path} exact>
                    {Icon || title}
                  </NavLink>
                </li>
              )),
            )}
          </ul>
        </nav>
      </div>
    );
  }
}
