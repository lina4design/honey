import firebase from "firebase";

const config = {
  apiKey           : "AIzaSyDEc9ix8UXtTG8tmzE7j903OpETgiIHgX0",
  authDomain       : "aido-bites.firebaseapp.com",
  databaseURL      : "https://aido-bites.firebaseio.com",
  projectId        : "aido-bites",
  storageBucket    : "aido-bites.appspot.com",
  messagingSenderId: "615026117868",
};
export default firebase.initializeApp(config);
